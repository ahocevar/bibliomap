# BiblioMap

BiblioMap ist die Büchereilandkarte Österreichs. Sie zeigt den Versorgungsgrad mit Büchereien auf Bezirksebene, und analysiert die Erfüllung bestimmter Ziele auf Österreich-, Bundesland- und Bezirksebene.

BiblioMap wurde im Auftrag des [Bibliothekenverband Österreichs](http://bvoe.at/) entwickelt. Die Webapplikation wurde mit der [OpenGeo Suite](http://opengeosuite.com/) umgesetzt.

## Installation

BiblioMap ist eine rein clientseitige JavaScript-Anwendung und beinhaltet auch die Daten, welche jedoch vom Administrator aktualisiert werden können.

Die jeweils aktuelle Version steht als [WAR-Datei](http://bitbucket.org/ahocevar/bibliomap/downloads/bibliomap.war) zum Download zur Verfügung. Diese Datei kann direkt im `webapps`-Verzeichnis eines Servlet Containers (z.B. Tomcat, Jetty) abgelegt werden. Alternativ kann die Datei mit einem unzip-Programm entpackt werden (ggf. vorher von `bibliomap.war` auf `bibliomap.zip` umbenennen). Der Inhalt des entpackten Ordners `WEB-INF/app` wird dann in ein beliebiges Verzeichnis auf einem Webserver (z.B. Apache, IIS) kopiert, und die Applikation wird durch Aufrufen von `index.html` gestartet.

Optional kann BiblioMap auch direkt aus dem Quellcode compiliert werden. Dazu werden Git, Java und Ant benötigt. Zum Compilieren wird im Wurzelverzeichnis `ant war` ausgeführt. Die fertige Applikation steht dann unter `build/bibliomap.war` zur Verfügung.

## Aktualisieren der Daten

Folgende Daten werden zur farblichen Darstellung und in den Info-Popups verarbeitet:

* `data/bezirke.csv` - Versorgungsgrad und statistische Daten
* `data/hoaustria.csv` - Zielerfüllungsmatrix Österreich, bezogen auf alle Gemeinden
* `data/hoaustriabib.csv` - Zielerfüllungsmatrix Österreich, bezogen auf Gemeinden mit Bibliotheken
* `data/hobuland.csv` - Zielerfüllungsmatrix Bundesländer, bezogen auf alle Gemeinden
* `data/hobulandbib.csv` - Zielerfüllungsmatrix Bundesländer, bezogen auf Gemeinden mit Bibliotheken
* `data/hobez.csv` - Zielerfüllungsmatrix Bezirke, bezogen auf alle Gemeinden
* `data/hobezbib.csv` - Zielerfüllungsmatrix Bezirke, bezogen auf Gemeinden mit Bibliotheken

Diese Dateien können jederzeit durch neue Versionen ersetzt werden, jedoch sind folgende Anforderungen zu beachten:

* Die Spaltennamen dürfen nicht geändert werden
* Die Reihenfolge der Spalten darf nicht geändert werden
* Die CSV-Dateien müssen mit Komma (,) als Trennzeichen und mit UTF-8 Encoding gespeichert werden

## Bedienung der Applikation

### Navigation

Der Baum auf der linken Seite dient zur Auswahl des Kartenthemas. Durch Klicken auf einen Radio-Button oder Doppelklick auf einen Kartennamen wird ein Kartenthema ausgewählt. Mit dem Pfeil links des Radio-Buttons kann die Legende ein- und ausgeblendet werden.

Oberhalb der Karte befindet sich eine Werkzeugleiste. Die Werkzeuge von links nach rechts haben folgend Funktion:

* Maximale Ausdehnung - Zoom auf ganz Österreich
* Vergrössern - Kartenausschnitt vergrößern (Zoom in)
* Verkleinern - Kartenausschnitt verkleinern (Zoom out)
* Kartenausschnitt zurück - Zum vorherigen Kartenausschnitt zurückkehren
* Kartenausschnitt vorwärts - Zur nächsten Kartenausschnitt weiterschalten

Weiters stehen links oben in der Karte Werkzeuge zum Zoomen und Verschieben des Kartenausschnitts zur Verfügung. Zoomen ist auch mit dem Mausrad möglich, und Verschieben der Karte erfolgt am bequemsten durch Ziehen mit gedrückter Maustaste. Durch Aufziehen eines Rechtecks mit der Maus bei gedrückter Shift-Taste kann gezielt zu einem Ausschnitt gezoomt werden.

Um die Fläche der Karte zu vergrößern, kann die Seitenleiste mit dem Baum eingeklappt werden. Dazu in der Mitte der Trennleiste zwischen Seitenleiste und Karte auf den kleinen Pfeil klicken.

### Interaktion mit der Karteninformation

Bei Bewegen des Mauszeigers über die Karte wird der Name des jeweiligen Bezirks oder Bundeslandes als Tooltip neben dem Mauszeiger angezeigt.

Bei Klicken in die Karte öffnet sich ein Popup mit den zum jeweiligen Kartenfeature zugeordneten Informationen. Manche Popups beinhalten Reiter mit unterschiedlichen Tabellen, zwischen denen umgeschaltet werden kann.

Popups können mit Klick auf das [X] geschlossen werden. Wird bei offenem Popup erneut auf einer anderen Stelle in dieselbe oder auch eine andere Karte geklickt, schließt sich das Popup. Um ein Popup offen zu halten oder es verschieben zu können, klickt man auf das Pin-Symbol nebem dem [X]. Damit kann man mehrere Popups öffnen und die Inhalte vergleichen.
