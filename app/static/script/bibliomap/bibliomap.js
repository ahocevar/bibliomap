/**
 * Add all your dependencies here.
 *
 * @require plugins/FeaturePopup.js
 * @require widgets/Viewer.js
 * @require plugins/LayerManager.js
 * @require plugins/OLSource.js
 * @require plugins/ZoomToExtent.js
 * @require plugins/NavigationHistory.js
 * @require plugins/Zoom.js
 * @require OpenLayers/Layer/Vector.js
 * @require OpenLayers/Renderer/Canvas.js
 * @require OpenLayers/Renderer/SVG.js
 * @require OpenLayers/Renderer/VML.js
 * @require OpenLayers/Strategy/Fixed.js
 * @require OpenLayers/Protocol/HTTP.js
 * @require OpenLayers/Format/GeoJSON.js
 * @require locale/de.js
 */

GeoExt.Lang.set("de");
OpenLayers.Layer.Vector.prototype.ratio = 3;
OpenLayers.Layer.Vector.prototype.attribution = '&copy; BVÖ, BMUKK';

// Workaround for maintaining the order of the rows.
var origInitComponent = Ext.grid.PropertyGrid.prototype.initComponent;
Ext.grid.PropertyGrid.prototype.initComponent = function() {
    var origSort = Ext.data.Store.prototype.sort;
    Ext.data.Store.prototype.sort = function() {};
    origInitComponent.apply(this, arguments);
    Ext.data.Store.prototype.sort = origSort;
};

// Array.prototype.indexOf for browsers that don't have it
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement) {
        return OpenLayers.Util.indexOf(this, searchElement);
    }
}

// Do not move selection layer to top
OpenLayers.Handler.Feature.prototype.moveLayerToTop = OpenLayers.Function.Void;

// Use Canvas randerer in Safari for Grenzen layer, because Safari does not let
// events through SVG even if pointer-events is set to none.
var renderers = ['SVG', 'Canvas', 'VML'];
if (/ Version\/[0-9\. ]+Safari\//.test(navigator.userAgent)) {
    renderers.shift();
}

var app = new gxp.Viewer({
    portalConfig: {
        layout: "border",
        region: "center",
        
        // by configuring items here, we don't need to configure portalItems
        // and save a wrapping container
        items: [{
            id: "centerpanel",
            xtype: "panel",
            layout: "fit",
            region: "center",
            border: false,
            items: ["mymap"]
        }, {
            id: "westpanel",
            xtype: "panel",
            border: false,
            layout: "fit",
            region: "west",
            width: 200,
            collapseMode: "mini",
            collapsible: true,
            resizable: true,
            header: false,
            split: true
        }],
        bbar: {id: "mybbar"}
    },
    
    // configuration of all tool plugins for this application
    tools: [{
        ptype: "gxp_layermanager",
        groups: {
            "basemap": {
                title: "Grundkarte",
                exclusive: true
            },
            "versorgung": {
                title: "Versorgungsgrad",
                exclusive: "content"
            },
            "default": {
                title: "Zielstandards",
                exclusive: "content"
            },
            "foerderung": {
                title: "Förderrichtlinien",
                exclusive: "content"
            }
        },
        outputConfig: {
            id: "tree",
            border: true,
            autoScroll: true,
            title: "Kartenthemen",
            listeners: {
                checkchange: function(node) {
                    var layer = node.attributes.layer;
                    var record = app.mapPanel.layers.getByLayer(layer);
                    if (layer.visibility) {
                        window.setTimeout(function() {
                            app.selectLayer(record);
                        }, 0);
                    }
                }
            }
        },
        outputTarget: "westpanel"
    }, {
        ptype: "gxp_zoomtoextent",
        extent: [1060986.912102,5840265.754779,1910466.318195,6278488.916805],
        closest: false,
        actionTarget: "map.tbar"
    }, {
        ptype: "gxp_zoom",
        actionTarget: "map.tbar"
    }, {
        ptype: "gxp_navigationhistory",
        actionTarget: "map.tbar"
    }, {
        ptype: "app_featurepopup"
    }],
    
    // layer sources
    sources: {
        ol: {
            ptype: "gxp_olsource"
        }
    },
    
    // map and layers
    map: {
        id: "mymap", // id needed to reference map in portalConfig above
        projection: "EPSG:900913",
        maxResolution: 1222.9924523925781,
        numZoomLevels: 4,
        extent: [1060986.912102,5840265.754779,1910466.318195,6278488.916805],
        layers: [{
            source: "ol",
            name: "versorgung_bezirke",
            group: "versorgung",
            type: "OpenLayers.Layer.Vector",
            args: ["Bezirke", {
                strategies: [new OpenLayers.Strategy.Fixed({
                    autoActivate: false
                })],
                protocol: new OpenLayers.Protocol.HTTP({
                    url: "data/bezirke.geojson",
                    format: new OpenLayers.Format.GeoJSON()
                })
            }],
            fixed: true
        }, {
            source: "ol",
            name: "versorgung_bundeslaender",
            group: "versorgung",
            type: "OpenLayers.Layer.Vector",
            args: ["Bundesländer", {
                strategies: [new OpenLayers.Strategy.Fixed({
                    autoActivate: false
                })],
                protocol: new OpenLayers.Protocol.HTTP({
                    url: "data/bundeslaender.geojson",
                    format: new OpenLayers.Format.GeoJSON()
                })
            }],
            fixed: true
        }, {
            source: "ol",
            name: "versorgung_austria",
            group: "versorgung",
            type: "OpenLayers.Layer.Vector",
            args: ["Österreich gesamt", {
                strategies: [new OpenLayers.Strategy.Fixed({
                    autoActivate: false
                })],
                protocol: new OpenLayers.Protocol.HTTP({
                    url: "data/oesterreich.geojson",
                    format: new OpenLayers.Format.GeoJSON()
                })
            }],
            fixed: true
        }, {
            source: "ol",
            name: "zielerfuellung_bezirke",
            type: "OpenLayers.Layer.Vector",
            args: ["Bezirke", {
                strategies: [new OpenLayers.Strategy.Fixed({
                    autoActivate: false
                })],
                protocol: new OpenLayers.Protocol.HTTP({
                    url: "data/bezirke.geojson",
                    format: new OpenLayers.Format.GeoJSON()
                }),
                visibility: false
            }],
            fixed: true
        }, {
            source: "ol",
            name: "zielerfuellung_bundeslaender",
            type: "OpenLayers.Layer.Vector",
            args: ["Bundesländer", {
                strategies: [new OpenLayers.Strategy.Fixed({
                    autoActivate: false
                })],
                protocol: new OpenLayers.Protocol.HTTP({
                    url: "data/bundeslaender.geojson",
                    format: new OpenLayers.Format.GeoJSON()
                }),
                visibility: false
            }],
            fixed: true
        }, {
            source: "ol",
            name: "zielerfuellung_austria",
            type: "OpenLayers.Layer.Vector",
            args: ["Österreich gesamt", {
                strategies: [new OpenLayers.Strategy.Fixed({
                    autoActiate: false
                })],
                protocol: new OpenLayers.Protocol.HTTP({
                    url: "data/oesterreich.geojson",
                    format: new OpenLayers.Format.GeoJSON()
                }),
                visibility: false
            }],
            fixed: true
          }, {
              source: "ol",
              name: "foerderung_bezirke",
              type: "OpenLayers.Layer.Vector",
              group: "foerderung",
              args: ["Bezirke", {
                  strategies: [new OpenLayers.Strategy.Fixed({
                      autoActivate: false
                  })],
                  protocol: new OpenLayers.Protocol.HTTP({
                      url: "data/bezirke.geojson",
                      format: new OpenLayers.Format.GeoJSON()
                  }),
                  visibility: false
              }],
              fixed: true
          }, {
              source: "ol",
              name: "foerderung_bundeslaender",
              type: "OpenLayers.Layer.Vector",
              group: "foerderung",
              args: ["Bundesländer", {
                  strategies: [new OpenLayers.Strategy.Fixed({
                      autoActivate: false
                  })],
                  protocol: new OpenLayers.Protocol.HTTP({
                      url: "data/bundeslaender.geojson",
                      format: new OpenLayers.Format.GeoJSON()
                  }),
                  visibility: false
              }],
              fixed: true
          }, {
              source: "ol",
              name: "foerderung_austria",
              type: "OpenLayers.Layer.Vector",
              group: "foerderung",
              args: ["Österreich gesamt", {
                  strategies: [new OpenLayers.Strategy.Fixed({
                      autoActiate: false
                  })],
                  protocol: new OpenLayers.Protocol.HTTP({
                      url: "data/oesterreich.geojson",
                      format: new OpenLayers.Format.GeoJSON()
                  }),
                  visibility: false
              }],
              fixed: true
        }, {
            source: "ol",
            type: "OpenLayers.Layer",
            args: ["Keine"],
            group: "basemap",
            fixed: true
        }, {
            source: "ol",
            type: "OpenLayers.Layer.Vector",
            args: ["Bundesländer", {
                strategies: [new OpenLayers.Strategy.Fixed()],
                protocol: new OpenLayers.Protocol.HTTP({
                    url: "data/bundeslaender.geojson",
                    format: new OpenLayers.Format.GeoJSON()
                }),
                styleMap: new OpenLayers.StyleMap(new OpenLayers.Style({}, {
                    rules: [
                        new OpenLayers.Rule({
                            title: "Grenzen",
                            symbolizer: Ext.applyIf({
                                fill: false,
                                strokeColor: "black",
                                strokeOpacity: 1,
                                strokeWidth: 2
                            }, OpenLayers.Feature.Vector.style["default"])
                        })
                    ]}
                )),
                renderers: renderers
            }],
            group: "basemap",
            fixed: true
        }],
        items: [{
            xtype: "gx_zoomslider",
            vertical: true,
            height: 100
        }]
    },
    listeners: {
        beforelayerselectionchange: function(record) {
            return record.get('group') !== 'basemap';
        },
        layerselectionchange: function(record) {
            record.getLayer().setVisibility(true);
        }
    }

});
