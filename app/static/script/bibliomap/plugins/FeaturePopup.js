/**
 * @require plugins/Tool.js
 * @require OpenLayers/Control/SelectFeature.js
 * @require OpenLayers/Filter.js
 * @require GeoExt/widgets/Popup.js
 */

Ext.namespace("bibliomap.plugins");

bibliomap.plugins.FeaturePopup = Ext.extend(gxp.plugins.Tool, {

    ptype: "app_featurepopup",

    /** api: config[outputTarget]
     *  ``String`` By default, the FeatureEditPopup will be added to the map.
     */
    outputTarget: "map",

    /** private: property[popup]
     *  :class:`gxp.FeatureEditPopup` FeatureEditPopup for this tool
     */
    popup: null,

    /** private: property[bezData]
     *  ``Array``
     */
    bezData: null,

    /** private: property[bezRows]
     *  ``Array``
     */
    bezRows: null,

    /** private: property[foebezData]
     *  ``Array``
     */
    foebezData: null,

    /** private: property[foebezRows]
     *  ``Array``
     */
    foebezRows: null,

    /** private: property[bulandData]
     *  ``Array``
     */
    bulandData: null,

    /** private: property[bulandRows]
     *  ``Array``
     */
    bulandRows: null,

    /** private: property[hobezData]
     *  ``Array``
     */
    hobezData: null,

    /** private: property[hobezRows]
     *  ``Array``
     */
    hobezRows: null,

    /** private: property[hobezbibData]
     *  ``Array``
     */
    hobezbibData: null,

    /** private: property[hobezbibRows]
     *  ``Array``
     */
    hobezbibRows: null,

    /** private: property[hobulandData]
     *  ``Array``
     */
    hobulandData: null,

    /** private: property[hobulandRows]
     *  ``Array``
     */
    hobulandRows: null,

    /** private: property[hobulandbibData]
     *  ``Array``
     */
    hobulandbibData: null,

    /** private: property[hobulandbibRows]
     *  ``Array``
     */
    hobulandbibRows: null,


    /** api: method[init]
     */
    init: function(target) {
        target.on("layerselectionchange", this.initControl, this);

        function csvToArray(text) {
            var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
            var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
            // Return NULL if input string is not well formed CSV string.
            if (!re_valid.test(text)) return null;
            var a = [];                     // Initialize array to receive values.
            text.replace(re_value, // "Walk" the string using replace with callback.
                function(m0, m1, m2, m3) {
                    // Remove backslash from \' in single quoted values.
                    if      (m1 !== undefined && m1 !== "") a.push(m1.replace(/\\'/g, "'"));
                    // Remove backslash from \" in double quoted values.
                    else if (m2 !== undefined && m2 !== "") a.push(m2.replace(/\\"/g, '"'));
                    else if (m3 !== undefined) a.push(m3);
                    return ''; // Return empty string.
                });
            // Handle special case of empty last value.
            if (/,\s*$/.test(text)) a.push('');
            return a;
        }

        var queue = [
            function(callback) {
                target.on("ready", callback, this);
            },
            function(callback) {
                Ext.Ajax.request({
                    method: "GET",
                    disableCaching: false,
                    url: "data/buland.csv",
                    success: function(response) {
                        var lines = response.responseText.split("\n"),
                            len = lines.length;
                        var data = {}, line,
                            headers = csvToArray(lines[0]);
                        for (var i=1; i<len; ++i) {
                            line = csvToArray(lines[i]);
                            if (!isNaN(line[0])) {
                                data[line[0]] = line;
                            }
                        }
                        this.bulandRows = csvToArray(lines[0]);
                        this.bulandData = data;
                        callback.call(this);
                    },
                    scope: this
                });
            },
            function(callback) {
                Ext.Ajax.request({
                    method: "GET",
                    disableCaching: false,
                    url: "data/bezirke.csv",
                    success: function(response) {
                        var lines = response.responseText.split("\n"),
                            len = lines.length;
                        var data = {}, line,
                            headers = csvToArray(lines[0]);
                        for (var i=1; i<len; ++i) {
                            line = csvToArray(lines[i]);
                            if (!isNaN(line[0])) {
                                data[line[0]] = line;
                            }
                        }
                        this.bezRows = csvToArray(lines[0]);
                        this.bezData = data;
                        callback.call(this);
                    },
                    scope: this
                });
            },
            function(callback) {
                Ext.Ajax.request({
                    method: "GET",
                    disableCaching: false,
                    url: "data/hobuland.csv",
                    success: function(response) {
                        var lines = response.responseText.split("\n"),
                            len = lines.length;
                        var data = {}, line;
                        for (var i=1; i<len; ++i) {
                            line = csvToArray(lines[i]);
                            if (line.length) {
                                data[line[0]] = line;
                            }
                        }
                        this.hobulandRows = csvToArray(lines[0]);
                        this.hobulandData = data;
                        callback.call(this);
                    },
                    scope: this
                });
            },
            function(callback) {
                Ext.Ajax.request({
                    method: "GET",
                    disableCaching: false,
                    url: "data/hobulandbib.csv",
                    success: function(response) {
                        var lines = response.responseText.split("\n"),
                            len = lines.length;
                        var data = {}, line;
                        for (var i=1; i<len; ++i) {
                            line = csvToArray(lines[i]);
                            if (line.length) {
                                data[line[0]] = line;
                            }
                        }
                        this.hobulandbibRows = csvToArray(lines[0]);
                        this.hobulandbibData = data;
                        callback.call(this);
                    },
                    scope: this
                });
            },
            function(callback) {
                Ext.Ajax.request({
                    method: "GET",
                    disableCaching: false,
                    url: "data/hobez.csv",
                    success: function(response) {
                        var lines = response.responseText.split("\n"),
                            len = lines.length;
                        var data = {}, line;
                        for (var i=1; i<len; ++i) {
                            line = csvToArray(lines[i]);
                            if (line.length) {
                                data[line[0]] = line;
                            }
                        }
                        this.hobezRows = csvToArray(lines[0]);
                        this.hobezData = data;
                        callback.call(this);
                    },
                    scope: this
                });
            },
            function(callback) {
                Ext.Ajax.request({
                    method: "GET",
                    disableCaching: false,
                    url: "data/hobezbib.csv",
                    success: function(response) {
                        var lines = response.responseText.split("\n"),
                            len = lines.length;
                        var data = {}, line;
                        for (var i=1; i<len; ++i) {
                            line = csvToArray(lines[i]);
                            if (line.length) {
                                data[line[0]] = line;
                            }
                        }
                        this.hobezbibRows = csvToArray(lines[0]);
                        this.hobezbibData = data;
                        callback.call(this);
                    },
                    scope: this
                });
            },
            function(callback) {
                Ext.Ajax.request({
                    method: "GET",
                    disableCaching: false,
                    url: "data/foebuland.csv",
                    success: function(response) {
                        var lines = response.responseText.split("\n"),
                            len = lines.length;
                        var data = {}, line,
                            headers = csvToArray(lines[0]);
                        for (var i=1; i<len; ++i) {
                            line = csvToArray(lines[i]);
                            if (!isNaN(line[0])) {
                                data[line[0]] = line;
                            }
                        }
                        this.foebulandRows = csvToArray(lines[0]);
                        this.foebulandData = data;
                        callback.call(this);
                    },
                    scope: this
                });
            },
            function(callback) {
                Ext.Ajax.request({
                    method: "GET",
                    disableCaching: false,
                    url: "data/foebez.csv",
                    success: function(response) {
                        var lines = response.responseText.split("\n"),
                            len = lines.length;
                        var data = {}, line,
                            headers = csvToArray(lines[0]);
                        for (var i=1; i<len; ++i) {
                            line = csvToArray(lines[i]);
                            if (!isNaN(line[0])) {
                                data[line[0]] = line;
                            }
                        }
                        this.foebezRows = csvToArray(lines[0]);
                        this.foebezData = data;
                        callback.call(this);
                    },
                    scope: this
                });
            }
        ];

        gxp.util.dispatch(queue, this.setupStyles, this);

        bibliomap.plugins.FeaturePopup.superclass.init.apply(this, arguments);
    },

    initControl: function() {
        var popup,
            target = this.target,
            map = target.mapPanel.map;
        if (this.selectControl) {
            this.selectControl.unselectAll();
            this.selectControl.destroy();
        }
        this.selectControl = new OpenLayers.Control.SelectFeature(target.selectedLayer.getLayer(), {
            eventListeners: {
                "featureunhighlighted": function(evt) {
                    if (popup && popup.isVisible() && !popup.header.hasClass("x-window-draggable")) {
                        popup.close();
                    }
                },
                "featurehighlighted": function(evt) {
                    var feature = evt.feature;
                    var popupContent = this.getPopupContent(evt.feature);
                    if (this.selectControl.active) {
                        popup = this.addOutput({
                            xtype: "gx_popup",
                            collapsible: true,
                            location: evt.feature,
                            title: popupContent.title,
                            width: 650,
                            height: 309,
                            layout: "fit",
                            items: popupContent.items.length === 1 ? popupContent.items : {
                                xtype: "tabpanel",
                                border: false,
                                activeTab: 0,
                                items: popupContent.items
                            },
                            listeners: {
                                "close": function() {
                                    if(feature.layer && feature.layer.selectedFeatures.indexOf(feature) !== -1) {
                                        this.selectControl.unselect(feature);
                                        popup = null;
                                    }
                                },
                                scope: this
                            }
                        });
                        this.popup = popup;
                    }
                },
                scope: this
            }
        });
        this.selectControl.handlers.feature.stopDown = false;
        map.addControl(this.selectControl);
        this.selectControl.activate();
    },

    getPopupContent: function(feature) {

        function renderer(value, metaData, record, rowIndex, colIndex) {
            var pre = "", post = "";
            if (rowIndex === 8 || (colIndex === 0 && rowIndex === 7)) {
                pre = "<i>";
                post = "</i>";
            }
            if (colIndex === 0 || rowIndex === 7) {
                pre += "<b>";
                post = "</b>" + post;
            }
            return pre + value + post;
        }

        function foeRenderer(value, metaData, record, rowIndex, colIndex) {
            var pre = "", post = "";
            if (rowIndex === 8 || (colIndex === 0 && rowIndex === 1)) {
                pre = "<i>";
                post = "</i>";
            }
            if (colIndex === 0 || rowIndex === 1) {
                pre += "<b>";
                post = "</b>" + post;
            }
            return pre + value + post;
        }

        function getBulandData(data, rows, proPrefix) {
            var headers = targetRows.concat(),
                matrix = [],
                i, j;
            for (j=1; j<=9; ++j) {
                matrix[j-1] = headers.splice(0, 1);
                for (i=1; i<=6; ++i) {
                    if (j < 8) {
                        matrix[j-1][i] = data[rows.indexOf("zeq"+i+""+j+"_bez_sum")];
                    } else if (j === 8) {
                        matrix[j-1][i] = data[rows.indexOf("zeq"+i+"_bez_sum")];
                    } else if (j === 9) {
                        matrix[j-1][i] = data[rows.indexOf(proPrefix+i+"pro")];
                    }
                }
            }
            return matrix;
        }

        function getBezData(data, rows, proPrefix) {
            var headers = targetRows.concat(),
                matrix = [],
                i, j, index;
                for (j=1; j<=9; ++j) {
                    matrix[j-1] = headers.splice(0, 1);
                    for (i=1; i<=6; ++i) {
                        if (j < 8) {
                            matrix[j-1][i] = data[rows.indexOf("zeq"+i+""+j+"_bez_sum")];
                        } else if (j === 8) {
                            matrix[j-1][i] = data[rows.indexOf("zeq"+i+"_bez_sum")];
                        } else if (j === 9) {
                            matrix[j-1][i] = data[rows.indexOf(proPrefix+i+"pro")];
                        }
                    }
                }
            return matrix;
        }

        function getFoeData(data, rows, proPrefix) {
            var headers = foeTargetRows.concat(),
                matrix = [],
                i, j, colIdx;
            for (j=0; j<=7; ++j) {
                matrix[j] = headers.splice(0, 1);
                for (i=1; i<=8; ++i) {
                    colIdx = i == 3 ? 1 : (i == 1 ? 3 : i);
                    colIdx = colIdx == 2 ? 1 : (colIdx == 1 ? 2 : colIdx);
                    if (j >= 2) {
                        matrix[j][colIdx] = data[rows.indexOf("foe"+(i-1)+""+(j-1)+"_sum")];
                    } else if (j == 0) {
                        matrix[j][colIdx] = data[rows.indexOf("bibs-"+(i-1))];
                    } else if (j == 1) {
                        matrix[j][colIdx] = data[rows.indexOf("foe"+(i-1)+"_sum")];
                    }
                }
            }
            return matrix;
        }

        var targetRows = [
            "Medien", "Erneuerung", "Raum", "Öffnungszeiten", "PC, Internet, Audio", "Personalstelle", "Fortbildung", "Gesamt", "Anteil Bevölkerung"
        ];
        var fields = [
            "Zielstandard",
            "_1500",
            "1500_2500",
            "2500_5000",
            "5000_10000",
            "10000_50000",
            "50000_"
        ];
        var columns = [
            {header: "", width: 110, mapping: "Zielstandard", renderer: renderer},
            {header: "bis 1.500 EW", mapping: "_1500", renderer: renderer},
            {header: "1.500-2.500 EW", mapping: "1500_2500", renderer: renderer},
            {header: "2.500-5.000 EW", mapping: "2500_5000", renderer: renderer},
            {header: "5.000-10.000 EW ¹", mapping: "5000_10000", renderer: renderer},
            {header: "10.000-50.000 EW", mapping: "10000_50000", renderer: renderer},
            {header: "über 50.000 EW", mapping: "50000_", renderer: renderer}
        ];

        var foeTargetRows = [
            "Anzahl Bibliotheken", "Anteil förderwürdig", "Ausbildung", "Umsatz", "Medienanzahl", "Öffnungsstunden", "Öffnungstage", "Erneuerungsquote"
        ];
        var foeFields = [
            "Kriterium",
            "-1500",
            "1500_2500",
            "Nebenversorger",
            "2500_5000",
            "5000_10000",
            "10000_50000",
            "50000_",
            "Summe"
        ];
        var foeColumns = [
            {header: "", width: 140, mapping: "Kriterium", renderer: foeRenderer},
            {header: "bis 1.500 EW", mapping: "_1500", renderer: foeRenderer},
            {header: "1.500-2.500 EW", mapping: "1500_2500", renderer: foeRenderer},
            {header: "Nebenversorger", mapping: "Nebenversorger", renderer: foeRenderer},
            {header: "2.500-5.000 EW", mapping: "2500_5000", renderer: foeRenderer},
            {header: "5.000-10.000 EW ¹", mapping: "5000_10000", renderer: foeRenderer},
            {header: "10.000-50.000 EW", mapping: "10000_50000", renderer: foeRenderer},
            {header: "über 50.000 EW", mapping: "50000_", renderer: foeRenderer}
        ];

        var bbar = ["¹ beinhaltet Bezirkshauptstädte mit weniger als 5.000 Einwohnern"];
        var data, dataBib, title, items;
        if (feature.layer === this.getLayer("versorgung_austria")) {
            data = this.bulandData[""];
            title = "Österreich gesamt<br>Versorgungsgrad: " + data[2];
            items = [{
                xtype: 'tabpanel',
                activeTab: 0,
                items: [{
                    xtype: 'propertygrid',
                    title: 'Bundesdaten',
                    hideHeaders: true,
                    border: false,
                    source: {
                        'Städte/Gemeinden': data[3],
                        'Städte/Gemeinden ohne Bibliothek': data[4],
                        'Bibliotheken': data[5],
                        '': '',
                        'EinwohnerInnen Österreichs': data[6],
                        'EinwohnerInnen in Städten/Gemeinden mit Bibliothek': data[7]
                    },
                    listeners: {
                        beforeEdit: function() { return false; }
                    }
                }, {
                    xtype: 'propertygrid',
                    title: 'BenutzerInnen',
                    hideHeaders: true,
                    border: false,
                    source: {
                        'Anzahl der BibliotheksbenutzerInnen': data[8],
                        '&nbsp;&nbsp;&nbsp;&nbsp;Österreich gesamt': data[9],
                        '&nbsp;&nbsp;&nbsp;&nbsp;Nur Städte/Gemeinden mit Bibliothek': data[10],
                        '': '',
                        'Entlehnungen pro EinwohnerIn': data[11],
                        '&nbsp;&nbsp;&nbsp; Österreich gesamt': data[12],
                        '&nbsp;&nbsp;&nbsp; Nur Städte/Gemeinden mit Bibliothek': data[13],
                        'Entlehnungen pro BenutzerIn': data[14]
                    },
                    listeners: {
                        beforeEdit: function() { return false; }
                    }
                }, {
                    xtype: 'propertygrid',
                    title: 'Medien',
                    hideHeaders: true,
                    border: false,
                    source: {
                        'Anzahl der Medien': data[15],
                        'Anzahl der Entlehnungen': data[16],
                        '': '',
                        'Medien pro EinwohnerIn': data[17],
                        '&nbsp;&nbsp;&nbsp; Österreich gesamt': data[18],
                        '&nbsp;&nbsp;&nbsp; Nur Städte/Gemeinden mit Bibliothek': data[19],
                        '&nbsp;': '',
                        'Entlehnungen pro Medium (Umsatz)': data[20]
                    },
                    listeners: {
                        beforeEdit: function() { return false; }
                    }
                }]
            }];
        } else if (feature.layer === this.getLayer("versorgung_bundeslaender")) {
            var data = this.bulandData[feature.attributes.land_id];
            title = "Bundesland: " + data[1] + "<br>Versorgungsgrad: " + data[2];
            items = [{
                xtype: 'tabpanel',
                activeTab: 0,
                items: [{
                    xtype: 'propertygrid',
                    title: 'Bundeslanddaten',
                    hideHeaders: true,
                    border: false,
                    source: {
                        'Städte/Gemeinden': data[3],
                        'Städte/Gemeinden ohne Bibliothek': data[4],
                        'Bibliotheken': data[5],
                        '': '',
                        'EinwohnerInnen des Bundeslandes': data[6],
                        'EinwohnerInnen in Städten/Gemeinden mit Bibliothek': data[7]
                    },
                    listeners: {
                        beforeEdit: function() { return false; }
                    }
                }, {
                    xtype: 'propertygrid',
                    title: 'BenutzerInnen',
                    hideHeaders: true,
                    border: false,
                    source: {
                        'Anzahl der BibliotheksbenutzerInnen': data[8],
                        '&nbsp;&nbsp;&nbsp;&nbsp;Gesamtes Bundesland': data[9],
                        '&nbsp;&nbsp;&nbsp;&nbsp;Nur Städte/Gemeinden mit Bibliothek': data[10],
                        '': '',
                        'Entlehnungen pro EinwohnerIn': data[11],
                        '&nbsp;&nbsp;&nbsp; Gesamtes Bundesland': data[12],
                        '&nbsp;&nbsp;&nbsp; Nur Städte/Gemeinden mit Bibliothek': data[13],
                        'Entlehnungen pro BenutzerIn': data[14]

                    },
                    listeners: {
                        beforeEdit: function() { return false; }
                    }
                }, {
                    xtype: 'propertygrid',
                    title: 'Medien',
                    hideHeaders: true,
                    border: false,
                    source: {
                        'Anzahl der Medien': data[15],
                        'Anzahl der Entlehnungen': data[16],
                        '': '',
                        'Medien pro EinwohnerIn': data[17],
                        '&nbsp;&nbsp;&nbsp; Gesamtes Bundesland': data[18],
                        '&nbsp;&nbsp;&nbsp; Nur Städte/Gemeinden mit Bibliothek': data[19],
                        '&nbsp;': '',
                        'Entlehnungen pro Medium (Umsatz)': data[20]
                    },
                    listeners: {
                        beforeEdit: function() { return false; }
                    }
                }]
            }];
        } else if (feature.layer === this.getLayer("versorgung_bezirke")) {
            var data = this.bezData[feature.attributes.bezirk_id];
            title = "Bezirk: " + data[1] + "<br>Versorgungsgrad: " + data[2];
            items = [{
                xtype: 'tabpanel',
                activeTab: 0,
                items: [{
                    xtype: 'propertygrid',
                    title: 'Bezirksdaten',
                    hideHeaders: true,
                    border: false,
                    source: {
                        'Städte/Gemeinden': data[3],
                        'Städte/Gemeinden ohne Bibliothek': data[4],
                        'Bibliotheken': data[5],
                        '': '',
                        'EinwohnerInnen des Bezirks': data[6],
                        'EinwohnerInnen in Städten/Gemeinden mit Bibliothek': data[7]
                    },
                    listeners: {
                        beforeEdit: function() { return false; }
                    }
                }, {
                    xtype: 'propertygrid',
                    title: 'BenutzerInnen',
                    hideHeaders: true,
                    border: false,
                    source: {
                        'Anzahl der BibliotheksbenutzerInnen': data[8],
                        '&nbsp;&nbsp;&nbsp;&nbsp;Gesamter Bezirk': data[9],
                        '&nbsp;&nbsp;&nbsp;&nbsp;Nur Städte/Gemeinden mit Bibliothek': data[10],
                        '': '',
                        'Entlehnungen pro EinwohnerIn': data[11],
                        '&nbsp;&nbsp;&nbsp; Gesamter Bezirk': data[12],
                        '&nbsp;&nbsp;&nbsp; Nur Städte/Gemeinden mit Bibliothek': data[13],
                        'Entlehnungen pro BenutzerIn': data[14]
                    },
                    listeners: {
                        beforeEdit: function() { return false; }
                    }
                }, {
                    xtype: 'propertygrid',
                    title: 'Medien',
                    hideHeaders: true,
                    border: false,
                    source: {
                        'Anzahl der Medien': data[15],
                        'Anzahl der Entlehnungen': data[16],
                        '': '',
                        'Medien pro EinwohnerIn': data[17],
                        '&nbsp;&nbsp;&nbsp; Gesamter Bezirk': data[18],
                        '&nbsp;&nbsp;&nbsp; Nur Städte/Gemeinden mit Bibliothek': data[19],
                        '&nbsp;': '',
                        'Entlehnungen pro Medium (Umsatz)': data[20]
                    },
                    listeners: {
                        beforeEdit: function() { return false; }
                    }
                }]
            }];
        } else if (feature.layer === this.getLayer("zielerfuellung_austria")) {
            data = this.hobulandData[''];
            dataBib = this.hobulandbibData[''];
            title = "Zielerfüllung Österreich";
            items = [{
                xtype: "grid",
                title: "Gemeinden mit Bibliotheken - " + dataBib[2],
                viewConfig: {forceFit: true},
                store: new Ext.data.ArrayStore({
                    idIndex: 0,
                    fields: fields,
                    data: getBulandData(dataBib, this.hobulandbibRows, "b")
                }),
                columns: columns,
                bbar: bbar
            }, {
                xtype: "grid",
                title: "Alle Gemeinden - " + data[2],
                viewConfig: {forceFit: true},
                store: new Ext.data.ArrayStore({
                    idIndex: 0,
                    fields: fields,
                    data: getBulandData(data, this.hobulandRows, "a")
                }),
                columns: columns,
                bbar: bbar
            }];
        } else if (feature.layer === this.getLayer("zielerfuellung_bundeslaender")) {
            data = this.hobulandData[feature.attributes.land_id];
            dataBib = this.hobulandbibData[feature.attributes.land_id];
            title = "Zielerfüllung " + data[1];
            items = [{
                xtype: "grid",
                title: "Gemeinden mit Bibliotheken - " + dataBib[2],
                viewConfig: {forceFit: true},
                store: new Ext.data.ArrayStore({
                    idIndex: 0,
                    fields: fields,
                    data: getBulandData(dataBib, this.hobulandbibRows, "b")
                }),
                columns: columns,
                bbar: bbar
            }, {
                xtype: "grid",
                title: "Alle Gemeinden - " + data[2],
                viewConfig: {forceFit: true},
                store: new Ext.data.ArrayStore({
                    idIndex: 0,
                    fields: fields,
                    data: getBulandData(data, this.hobulandRows, "a")
                }),
                columns: columns,
                bbar: bbar
            }];
        } else if (feature.layer === this.getLayer("zielerfuellung_bezirke")) {
            data = this.hobezData[feature.attributes.bezirk_id];
            dataBib = this.hobezbibData[feature.attributes.bezirk_id];
            title = "Zielerfüllung - Bezirk " + this.bezData[feature.attributes.bezirk_id][1];
            items = [{
                xtype: "grid",
                title: "Gemeinden mit Bibliotheken - " + dataBib[2],
                viewConfig: {forceFit: true},
                store: new Ext.data.ArrayStore({
                    idIndex: 0,
                    fields: fields,
                    data: getBezData(dataBib, this.hobezbibRows, "b")
                }),
                columns: columns,
                bbar: bbar
            }, {
                xtype: "grid",
                title: "Alle Gemeinden - " + data[2],
                viewConfig: {forceFit: true},
                store: new Ext.data.ArrayStore({
                    idIndex: 0,
                    fields: fields,
                    data: getBezData(data, this.hobezRows, "a")
                }),
                columns: columns,
                bbar: bbar
            }];
        } else if (feature.layer === this.getLayer("foerderung_austria")) {
            data = this.foebulandData[''];
            title = "Förderrichtlinien Österreich";
            items = [{
                xtype: "grid",
                title: "Anteil der förderwürdigen Bibliotheken: " + data[this.foebulandRows.indexOf('foe-ges')],
                viewConfig: {forceFit: true},
                store: new Ext.data.ArrayStore({
                    idIndex: 0,
                    fields: foeFields,
                    data: getFoeData(data, this.foebulandRows, "a")
                }),
                columns: foeColumns,
                bbar: bbar
            }];
        } else if (feature.layer === this.getLayer("foerderung_bundeslaender")) {
            data = this.foebulandData[feature.attributes.land_id];
            title = "Förderrichtlinien " + data[this.foebulandRows.indexOf('Bundesland')];
            items = [{
                xtype: "grid",
                title: "Anteil der förderwürdigen Bibliotheken: " + data[this.foebulandRows.indexOf('foe-ges')],
                viewConfig: {forceFit: true},
                store: new Ext.data.ArrayStore({
                    idIndex: 0,
                    fields: foeFields,
                    data: getFoeData(data, this.foebulandRows, "a")
                }),
                columns: foeColumns,
                bbar: bbar
            }];
        } else if (feature.layer === this.getLayer("foerderung_bezirke")) {
            data = this.foebezData[feature.attributes.bezirk_id];
            title = "Förderrichtlinien " + data[this.foebezRows.indexOf('Bezirk')];
            items = [{
                xtype: "grid",
                title: "Anteil der förderwürdigen Bibliotheken: " + data[this.foebezRows.indexOf('foe-ges')],
                viewConfig: {forceFit: true},
                store: new Ext.data.ArrayStore({
                    idIndex: 0,
                    fields: foeFields,
                    data: getFoeData(data, this.foebezRows, "a")
                }),
                columns: foeColumns,
                bbar: bbar
            }];
        }
        return {
            items: items,
            title: title
        };
    },

    getLayer: function(name) {
        return this.target.getLayerRecordFromMap({
            name: name, source: "ol"
        }).getLayer();
    },

    setupStyles: function() {
        var versorgungAustria = this.getLayer("versorgung_austria");
        var bulandData = this.bulandData;
        var styleMap = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style(null, {
                context: {
                    getTitle: function(feature) {
                        return "Österreich";
                    }
                },
                rules: this.createRules(function(attributes) {
                    var land = bulandData[""];
                    if (land) {
                        var percent = parseFloat(land[2]);
                        return percent >= this.lowerBound && percent < this.upperBound;
                    }
                }, true)
            }),
            "select": new OpenLayers.Style(OpenLayers.Feature.Vector.style.select)
        });
        versorgungAustria.styleMap = styleMap;
        versorgungAustria.strategies[0].activate();
        this.target.selectLayer(this.target.mapPanel.layers.getByLayer(versorgungAustria));

        var versorgungBundeslaender = this.getLayer("versorgung_bundeslaender");
        var bulandData = this.bulandData;
        var styleMap = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style(null, {
                context: {
                    getTitle: function(feature) {
                        var land = bulandData[feature.attributes.land_id];
                        return land ? land[1] : "";
                    }
                },
                rules: this.createRules(function(attributes) {
                    var land = bulandData[attributes.land_id];
                    if (land) {
                        var percent = parseFloat(land[2]);
                        return percent >= this.lowerBound && percent < this.upperBound;
                    }
                }, true)
            }),
            "select": new OpenLayers.Style(OpenLayers.Feature.Vector.style.select)
        });
        versorgungBundeslaender.styleMap = styleMap;
        versorgungBundeslaender.strategies[0].activate();

        var versorgungBezirke = this.getLayer("versorgung_bezirke");
        var bezData = this.bezData;
        var styleMap = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style(null, {
                context: {
                    getTitle: function(feature) {
                        var bez = bezData[feature.attributes.bezirk_id];
                        return bez ? bez[1] : "";
                    }
                },
                rules: this.createRules(function(attributes) {
                    var bez = bezData[attributes.bezirk_id];
                    if (bez) {
                        var percent = parseFloat(bez[2]);
                        return percent >= this.lowerBound && percent < this.upperBound;
                    }
                }, true)
            }),
            "select": new OpenLayers.Style(OpenLayers.Feature.Vector.style.select)
        });
        versorgungBezirke.styleMap = styleMap;
        versorgungBezirke.strategies[0].activate();

        var hobulandData = this.hobulandbibData;

        var zielerfuellungAustria = this.getLayer("zielerfuellung_austria");
        styleMap = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style(null, {
                context: {
                    getTitle: function(feature) {
                        return "Österreich";
                    }
                },
                rules: this.createRules(function(attributes) {
                    var at = hobulandData[''];
                    if (at) {
                        var ze = parseFloat(at[2]);
                        return ze >= this.lowerBound && ze < this.upperBound;
                    }
                })
            }),
            "select": new OpenLayers.Style(OpenLayers.Feature.Vector.style.select)
        });
        zielerfuellungAustria.styleMap = styleMap;
        zielerfuellungAustria.strategies[0].activate();

        var zielerfuellungBundeslaender = this.getLayer("zielerfuellung_bundeslaender");
        styleMap = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style(null, {
                context: {
                    getTitle: function(feature) {
                        var land = hobulandData[feature.attributes.land_id];
                        return land ? land[1] : "";
                    }
                },
                rules: this.createRules(function(attributes) {
                    var land = hobulandData[attributes.land_id];
                    if (land) {
                        var ze = parseFloat(land[2]);
                        return ze >= this.lowerBound && ze < this.upperBound;
                    }
                })
            }),
            "select": new OpenLayers.Style(OpenLayers.Feature.Vector.style.select)
        });
        zielerfuellungBundeslaender.styleMap = styleMap;
        zielerfuellungBundeslaender.strategies[0].activate();

        var zielerfuellungBezirke = this.getLayer("zielerfuellung_bezirke");
        var hobezData = this.hobezbibData;
        styleMap = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style(null, {
                context: {
                    getTitle: function(feature) {
                        var bez = bezData[feature.attributes.bezirk_id];
                        return bez ? bez[1] : "";
                    }
                },
                rules: this.createRules(function(attributes) {
                    var bez = hobezData[attributes.bezirk_id];
                    if (bez) {
                        var ze = parseFloat(bez[2]);
                        return ze >= this.lowerBound && ze < this.upperBound;
                    }
                })
            }),
            "select": new OpenLayers.Style(OpenLayers.Feature.Vector.style.select)
        });
        zielerfuellungBezirke.styleMap = styleMap;
        zielerfuellungBezirke.strategies[0].activate();

        var foerderungAustria = this.getLayer("foerderung_austria");
        var foebulandData = this.foebulandData;
        var foebulandGesCol = this.foebulandRows.indexOf('foe-ges');
        styleMap = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style(null, {
                context: {
                    getTitle: function(feature) {
                        return "Österreich";
                    }
                },
                rules: this.createRules(function(attributes) {
                    var at = foebulandData[''];
                    if (at) {
                        var foe = parseFloat(at[foebulandGesCol]);
                        return foe >= this.lowerBound && foe < this.upperBound;
                    }
                })
            }),
            "select": new OpenLayers.Style(OpenLayers.Feature.Vector.style.select)
        });
        foerderungAustria.styleMap = styleMap;
        foerderungAustria.strategies[0].activate();

        var foerderungBundeslaender = this.getLayer("foerderung_bundeslaender");
        styleMap = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style(null, {
                context: {
                    getTitle: function(feature) {
                        var land = foebulandData[feature.attributes.land_id];
                        return land ? land[1] : "";
                    }
                },
                rules: this.createRules(function(attributes) {
                    var land = foebulandData[attributes.land_id];
                    if (land) {
                        var foe = parseFloat(land[foebulandGesCol]);
                        return foe >= this.lowerBound && foe < this.upperBound;
                    }
                })
            }),
            "select": new OpenLayers.Style(OpenLayers.Feature.Vector.style.select)
        });
        foerderungBundeslaender.styleMap = styleMap;
        foerderungBundeslaender.strategies[0].activate();

        var foerderungBezirke = this.getLayer("foerderung_bezirke");
        var foebezData = this.foebezData;
        var foebezGesRow = this.foebezRows.indexOf('foe-ges');
        styleMap = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style(null, {
                context: {
                    getTitle: function(feature) {
                        var bez = foebezData[feature.attributes.bezirk_id];
                        return bez ? bez[1] : "";
                    }
                },
                rules: this.createRules(function(attributes) {
                    var bez = foebezData[attributes.bezirk_id];
                    if (bez) {
                        var foe = parseFloat(bez[foebezGesRow]);
                        return foe >= this.lowerBound && foe < this.upperBound;
                    }
                })
            }),
            "select": new OpenLayers.Style(OpenLayers.Feature.Vector.style.select)
        });
        foerderungBezirke.styleMap = styleMap;
        foerderungBezirke.strategies[0].activate();
    },

    createRules: function(evaluate) {
        var style = OpenLayers.Util.applyDefaults({
            graphicTitle: "${getTitle}",
            fillOpacity: 1,
            strokeWidth: 1,
            strokeOpacity: 0.5,
            strokeColor: "white",
            cursor: "pointer",
            pointerEvents: "visiblePainted"
        }, OpenLayers.Feature.Vector.style["default"]);
        return [
            new OpenLayers.Rule({
                title: "80 bis 100 %",
                symbolizer: Ext.applyIf({fillColor: "#a40d14"}, style),
                filter: new OpenLayers.Filter({
                    lowerBound: 80,
                    upperBound: Number.POSITIVE_INFINITY,
                    evaluate: evaluate
                })
            }),
            new OpenLayers.Rule({
                title: "60 bis 80 %",
                symbolizer: Ext.applyIf({fillColor: "#ee1c25"}, style),
                filter: new OpenLayers.Filter({
                    lowerBound: 60,
                    upperBound: 80,
                    evaluate: evaluate
                })
            }),
            new OpenLayers.Rule({
                title: "40 bis 60 %",
                symbolizer: Ext.applyIf({fillColor: "#85a562"}, style),
                filter: new OpenLayers.Filter({
                    lowerBound: 40,
                    upperBound: 60,
                    evaluate: evaluate
                })
            }),
            new OpenLayers.Rule({
                title: "20 bis 40 %",
                symbolizer: Ext.applyIf({fillColor: "#6f7072"}, style),
                filter: new OpenLayers.Filter({
                    lowerBound: 20,
                    upperBound: 40,
                    evaluate: evaluate
                })
            }),
            new OpenLayers.Rule({
                title: "0 bis 20 %",
                symbolizer: Ext.applyIf({fillColor: "#b3b4b8"}, style),
                filter: new OpenLayers.Filter({
                    lowerBound: Number.NEGATIVE_INFINITY,
                    upperBound: 20,
                    evaluate: evaluate
                })
            })
        ];
    }

});

Ext.preg(bibliomap.plugins.FeaturePopup.prototype.ptype, bibliomap.plugins.FeaturePopup);
